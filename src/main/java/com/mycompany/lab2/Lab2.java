/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package com.mycompany.lab2;

import java.util.Scanner;

/**
 *
 * @author Tauru
 */
public class Lab2 {

    static void printWelcome() {
        System.out.println("Welcome to OX");
    }
    static String[][] table = {{"7", "8", "9"},
    {"4", "5", "6"},
    {"1", "2", "3"}
    };

    static void printTable() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(table[i][j] + " ");
            }
            System.out.println();
        }
    }
    static String playerTurn = "X";

    static void showPlayerturn() {
        System.out.println(playerTurn + " Turn");
    }

    static void inputNumber() {
        Scanner ip = new Scanner(System.in);
        while (true) {
            System.out.print("Please input number:");
            String N = ip.next();
            boolean checkInput = false;
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 3; j++) {
                    if (N.equals(table[i][j])) {
                        checkInput = true;
                        table[i][j] = playerTurn;
                        break;
                    }
                }
            }
            if (checkInput) {
                break;
            }
            if (!checkInput) {
                continue;
            }
            break;
        }
    }

    static void switchPlayer() {
        if (playerTurn.equals("X")) {
            playerTurn = "O";
        } else {
            playerTurn = "X";
        }
    }

    static boolean isWin() {
        if (checkWin()) {
            return true;
        }
        return false;
    }

    static boolean isDraw() {
        if (checkDraw()) {
            return true;
        }
        return false;
    }

    static void showWinner() {
        System.out.println(playerTurn + " Win");
    }

    static void showDraw() {
        System.out.println("Draw");
    }

    static boolean checkWin() {
        // Check rows
        for (int i = 0; i < 3; i++) {
            if (table[i][0].equals(table[i][1]) && table[i][0].equals(table[i][2])) {
                return true;
            }
        }
        // Check columns
        for (int j = 0; j < 3; j++) {
            if (table[0][j].equals(table[1][j]) && table[0][j].equals(table[2][j])) {
                return true;
            }
        }
        // Check diagonals
        if ((table[0][0].equals(table[1][1]) && table[0][0].equals(table[2][2]))
                || (table[0][2].equals(table[1][1]) && table[0][2].equals(table[2][0]))) {
            return true;
        }
        return false;
    }
    
    static boolean checkDraw(){
        for(int i = 0;i<3;i++) {
            for (int j = 0; j < 3; j++) {
                if (!table[i][j].equals("X") && !table[i][j].equals("O")) {
                    return false;
                }
            }
        }
        
        return true;
    }
    static void inputContinue(){
        Scanner ip = new Scanner(System.in);
        while (true) {
            System.out.print("Do you want to continue (yes or no): ");
            String ans = ip.next();
            if (ans.equalsIgnoreCase("yes")) {
                resetGame();
                break;
            }
            if (ans.equalsIgnoreCase("no")) {
                System.out.println("Thank you for playing.");
                System.exit(0);
            }
        }
    }
    static void resetGame(){
        table = new String[][]{{"7", "8", "9"},
    {"4", "5", "6"},
    {"1", "2", "3"}
    };
        showPlayerturn();
        
    }

    public static void main(String[] args) {
        printWelcome();
        while (true) {
            printTable();
            showPlayerturn();
            inputNumber();
            if (isWin()) {
                printTable();
                showWinner();
                inputContinue();
                break;
            }
            if (isDraw()) {
                printTable();
                showDraw();
                inputContinue();
                break;
            }
            switchPlayer();
        }
    }
}
